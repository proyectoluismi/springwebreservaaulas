package com.salesianostriana.dam.gestiondeaulas;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.salesianostriana.dam.gestiondeaulas.model.Configuracion;
import com.salesianostriana.dam.gestiondeaulas.model.Espacio;
import com.salesianostriana.dam.gestiondeaulas.model.FechaEspecial;
import com.salesianostriana.dam.gestiondeaulas.model.Reserva;
import com.salesianostriana.dam.gestiondeaulas.model.Usuario;
import com.salesianostriana.dam.gestiondeaulas.service.ConfiguracionService;
import com.salesianostriana.dam.gestiondeaulas.service.EspacioServicio;
import com.salesianostriana.dam.gestiondeaulas.service.FechaEspecialServicio;
import com.salesianostriana.dam.gestiondeaulas.service.ReservaServicio;
import com.salesianostriana.dam.gestiondeaulas.service.UsuarioServicio;

@SpringBootApplication
public class GestionDeAulasApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionDeAulasApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner init(BCryptPasswordEncoder passwordEncoder,ReservaServicio servicioReserva, EspacioServicio servicioEspacio, UsuarioServicio servicioUsuario, ConfiguracionService servicioConfig, FechaEspecialServicio servicioFechaEspecial) {
		return args -> {
			
			/*
			 * Bucle que encripta todas las contraseñas de los usuarios al iniciar el programa
			 */
			for (Usuario u : servicioUsuario.findAll()) {
				
					u.setPassword(passwordEncoder.encode(u.getPassword()));
					servicioUsuario.edit(u);
				
			}
			
		};
	}
}
