package com.salesianostriana.dam.gestiondeaulas.configuracion;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Utilizada para la configuración de la seguridad
 * @author Daniel Leiva Nieto
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {
	
	/**
	 * Bean relacionado con la internacionalización. Aquí establecemos el idioma por defecto
	 * @return
	 */
	@Bean
	public LocaleResolver localeResolver() {
	    SessionLocaleResolver slr = new SessionLocaleResolver();
	    //slr.setDefaultLocale(new Locale("en","EN"));
	    slr.setDefaultLocale(Locale.getDefault());

	    return slr;
	}
	
	/**
	 * Bean relacionado con la internacionalización. Establece el parameto lang para los idiomas
	 * @return
	 */
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
	    LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
	    lci.setParamName("lang");
	    return lci;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(localeChangeInterceptor());
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login");
		registry.addViewController("/acceso-denegado");
		
	}
}
