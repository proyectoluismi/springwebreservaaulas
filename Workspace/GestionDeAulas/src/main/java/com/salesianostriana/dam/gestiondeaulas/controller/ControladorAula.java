/**
 * 
 */
package com.salesianostriana.dam.gestiondeaulas.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.salesianostriana.dam.gestiondeaulas.model.Espacio;
import com.salesianostriana.dam.gestiondeaulas.service.EspacioServicio;

/**
 * Esta clase maneja todos los controller sobre los espacios disponibles de la aplicación
 * @author Pablo Rodríguez Roldán
 *
 */
@Controller
public class ControladorAula {
	
	@Autowired
	EspacioServicio servicioEspacio;

	/**
	 * Controlador que muestra la página con el formulario para añadir un nuevo aula
	 * @param model
	 * @return
	 */
	@GetMapping({ "/formAula" })
	public String formularioAula(Model model) {

		Espacio espacio = new Espacio();
		model.addAttribute("EspacioForm", espacio);

		return "registroAula.html";

	}

	/**
	 * Controlador que guarda el nuevo aula que se ha creado con el formulario
	 * @param espacio
	 * @param model
	 * @return
	 */
	@PostMapping({ "/addAula" })
	public String addEspacio(@ModelAttribute("EspacioForm") Espacio espacio, Model model) {

		servicioEspacio.save(espacio);
		return "redirect:/listaulas";

	}

	/**
	 * Método que deshabilita un espacio por su id
	 * 
	 * @param idEspacio
	 * @return 
	 */
	@GetMapping("/deshabilitarEspacio/{id}")
	public String deshabilitarEspacio(@PathVariable("id") long id) {
		Espacio espacio = servicioEspacio.findById(id);
		espacio.setDisponible(false);
		servicioEspacio.edit(espacio);

		return "redirect:/listaulas";
	}
	
	/**
	 * Método que habilita un espacio por su id
	 * 
	 * @param idEspacio
	 * @return 
	 */
	@GetMapping("/habilitarEspacio/{id}")
	public String habilitarEspacio(@PathVariable("id") long id) {
		
		Espacio espacio = servicioEspacio.findById(id);
		espacio.setDisponible(true);
		servicioEspacio.edit(espacio);
		
		return "redirect:/listaulas";
	}

	/**
	 * Método para editar una cita por su id
	 * 
	 * @param id    identificador de la cita
	 * @param model representación de la información con la cual el sistema opera
	 * @return formulario de cita
	 */
	@GetMapping("/editarAula/{id}")
	public String mostrarFormularioEdicion(@PathVariable("id") long id, Model model) {

		Espacio aEditar = servicioEspacio.findById(id);

		if (aEditar != null) {
			model.addAttribute("EspacioForm", aEditar);
			return "registroAula";
		} else {
			return "redirect:/listaulas";
		}

	}

	/**
	 * Método que procesa la respuesta del formulario al editar
	 * 
	 * @param c cita que se va a editar
	 * @return nos redirecciona a la lista de citas
	 */
	@PostMapping("/editarAula/submit")
	public String procesarFormularioEdicion(@ModelAttribute("EspacioForm") Espacio espacio) {
		servicioEspacio.edit(espacio);
		return "redirect:/listaulas";
	}
	
	/**
	 * Método que manda a la página en la que se encuentran las aulas disponibles en el colegio
	 * @param model
	 * @param principal
	 * @return
	 */
	@GetMapping ({"/aulas",})
	public String verAulas(Model model,Principal principal) {
		
		List<Espacio> listaEspacios = servicioEspacio.findAll();
		
		model.addAttribute("listaEspacios", listaEspacios);
		
		return "aulasDisponibles";
	}
	
	

}
