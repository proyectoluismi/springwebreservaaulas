package com.salesianostriana.dam.gestiondeaulas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.salesianostriana.dam.gestiondeaulas.model.Usuario;
import com.salesianostriana.dam.gestiondeaulas.service.UsuarioServicio;


/**
 * Esta clase sirve para mostrar la plantilla del formulario y recoger sus datos para guardarlos en la bbdd
 * @author Daniel Leiva Nieto
 */
@Controller
public class ControladorFormulario {
	
	@Autowired
	UsuarioServicio servicioUsuario;

	/**
	 * Controlador que muestra la plantilla del formulario
	 * @param model
	 * @return
	 */
	@GetMapping ({"/registro"})
	public String showForm(Model model) {
		
		Usuario usuario = new Usuario();
		model.addAttribute("usuarioForm", usuario);
		
		return "registro";
		
	}
	
	/**
	 * Controlador que recoge los datos de /formulario y crea el nuevo usuario
	 * @param usuario
	 * @param model
	 * @return
	 */
	@PostMapping ("/addUsuario")
	public String submit(@ModelAttribute("usuarioForm") Usuario usuario,  Model model,BCryptPasswordEncoder passwordEncoder) {
		
		/*boolean error=false;
		
		resultado = (condicion)?valor1:valor2;
		for (Usuario u : servicioUsuario.findAll()) {
			if (u.getEmail().equals(usuario.getEmail())) {
				error=true;
			}
		}
		
		if (error) {
			
			return "acceso-denegado";
			
		}else {
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
			servicioUsuario.save(usuario);
			
			return "redirect:/";
			
		}*/
		usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
		servicioUsuario.save(usuario);
		return "redirect:/login";
		
		
	}
	
}
