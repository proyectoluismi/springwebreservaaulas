package com.salesianostriana.dam.gestiondeaulas.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.salesianostriana.dam.gestiondeaulas.model.Configuracion;
import com.salesianostriana.dam.gestiondeaulas.model.Espacio;
import com.salesianostriana.dam.gestiondeaulas.model.FechaEspecial;
import com.salesianostriana.dam.gestiondeaulas.model.Pager;
import com.salesianostriana.dam.gestiondeaulas.model.Reserva;
import com.salesianostriana.dam.gestiondeaulas.model.Usuario;
import com.salesianostriana.dam.gestiondeaulas.repository.FechaEspecialRepositorio;
import com.salesianostriana.dam.gestiondeaulas.service.ConfiguracionService;
import com.salesianostriana.dam.gestiondeaulas.service.EspacioServicio;
import com.salesianostriana.dam.gestiondeaulas.service.FechaEspecialServicio;
import com.salesianostriana.dam.gestiondeaulas.service.ReservaServicio;
import com.salesianostriana.dam.gestiondeaulas.service.UsuarioServicio;

/**
 * Controlador para establecer las rutas principales de la aplicacion
 * @author Daniel Leiva Nieto
 *
 */
@Controller
public class ControladorNavegacion {
	
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20, 50 };
	
	@Autowired
	UsuarioServicio servicioUsuario;
	
	@Autowired
	ReservaServicio servicioReserva;
	
	@Autowired
	EspacioServicio servicioEspacio;
	
	@Autowired
	ConfiguracionService servicioConfig;
	
	@Autowired
	FechaEspecialServicio servicioFechaEspecial;
	
	/**
	 * Controlador que devuelve la página principal de la aplicación
	 * @param model
	 * @return
	 */
	@GetMapping ({"/", "/inicio"})
	public String irAInicio(Model model,Principal principal) {
		
		//Si quiero ver los detalles del usuario en vez del email del UserDetails uso este método y 
		//añado esto en thymeleaf: th:text="${logeado.nombre + ' ' + logeado.apellidos}"
		
		//model.addAttribute("logeado", servicioUsuario.buscarUsuarioLogeado(principal));
		
		List<Reserva> listaReservaPendientes = servicioReserva.buscarReservasPendientesPorUsuario(servicioUsuario.buscarUsuarioLogeado(principal));
		List<Reserva> listaReservaPasadas = servicioReserva.buscarReservasPasadasPorUsuario(servicioUsuario.buscarUsuarioLogeado(principal));
		
		model.addAttribute("listaReservas", listaReservaPendientes);
		model.addAttribute("listaReservasPasadas", listaReservaPasadas);
		
		return "tusreservas";
	}
	
	/**
	 * Controlador que te dirige al login
	 * @param model
	 * @return
	 */
	@GetMapping ({"/login"})
	public String irALogin(Model model) {
		
		return "login";
	}
	
	/**
	 * Te dirige a la página de admin que puede decidir que días son hábiles
	 * @param model
	 * @return
	 */
	@GetMapping ({"/gestionDiasHabiles"})
	public String irAGestionDiasHabiles(Model model) {
		List<Configuracion> listaDias = servicioConfig.findAll();
		
		
		model.addAttribute("listaDias", listaDias);
		return "gestionDiasHabiles";
	}
	
	/**
	 * Te dirige a la página de admin que puede decidir que días son especiales
	 * @param model
	 * @return
	 */
	@GetMapping ({"/gestionFechasEspeciales"})
	public String irAGestionFechasEspeciales(Model model) {
		List<FechaEspecial> listaFechas = servicioFechaEspecial.findAll();
		
		
		model.addAttribute("listaFechas", listaFechas);
		return "gestionFechasEspeciales";
	}
	
	/**
	 * Controlador que elimina una fecha especial
	 * 
	 * @param idEspacio
	 * @return 
	 */
	@GetMapping("/eliminarFecha/{id}")
	public String eliminarFechaEspecial(@PathVariable("id") long id) {
		FechaEspecial fecha = servicioFechaEspecial.findById(id);
		servicioFechaEspecial.delete(fecha);

		return "redirect:/gestionFechasEspeciales";
	}
	
	/**
	 * Controlador que añade una fecha especial al calendario
	 * @param model
	 * @return
	 */
	@GetMapping({ "/anadirFechaEspecial" })
	public String formularioAula(Model model) {

		FechaEspecial fecha = new FechaEspecial();
		model.addAttribute("fechaForm", fecha);

		return "anadirFechaEspecial";

	}

	/**
	 * Controlador que guarda la fecha especial y te redirige a la página de gestionFechasEspeciales
	 * @param fecha
	 * @param model
	 * @return
	 */
	@PostMapping({ "/addFecha" })
	public String addEspacio(@ModelAttribute("fechaForm") FechaEspecial fecha, Model model) {
		
		servicioFechaEspecial.save(fecha);
		
		return "redirect:/gestionFechasEspeciales";

	}
	/**
	 * Método que deshabilita un día hábil
	 * 
	 * @param idEspacio
	 * @return 
	 */
	@GetMapping("/deshabilitarDia/{id}")
	public String deshabilitarDia(@PathVariable("id") long id) {
		Configuracion dia = servicioConfig.findById(id);
		dia.setHabilitado(false);
		servicioConfig.edit(dia);

		return "redirect:/gestionDiasHabiles";
	}
	
	/**
	 * Método que habilita un día hábil
	 * 
	 * @param idEspacio
	 * @return 
	 */
	@GetMapping("/habilitarDia/{id}")
	public String habilitarDia(@PathVariable("id") long id) {
		
		Configuracion dia = servicioConfig.findById(id);
		dia.setHabilitado(true);
		servicioConfig.edit(dia);

		return "redirect:/gestionDiasHabiles";
	}
	
	//ALBERTO
	
	/**
	 * Controlador que muestra la página en la que se encuentra la lista de reservas pendientes
	 * de todos los usuarios de la app
	 * @param pageSize
	 * @param page
	 * @param model
	 * @return
	 */
	@GetMapping ({"/gestionReservasPendientes"})
	public String irAGestionReservas(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page,Model model) {
		
			// Evalúa el tamaño de página. Si el parámetro es "nulo", devuelve
			// el tamaño de página inicial.
			int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
									
			// Calcula qué página se va a mostrar. Si el parámetro es "nulo" o menor
			// que 0, se devuelve el valor inicial. De otro modo, se devuelve el valor
			// del parámetro decrementado en 1.
			int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
									
			// Obtenemos la página definida por evalPage y evalPageSize de objetos de
			// nuestro modelo
			Page<Reserva> persons = servicioReserva.listaTodasReservasPendientes(PageRequest.of(evalPage, evalPageSize));
									
			// Creamos el objeto Pager (paginador) indicando los valores correspondientes.
			// Este sirve para que la plantilla sepa cuantas páginas hay en total, cuantos
			// botones
			// debe mostrar y cuál es el número de objetos a dibujar.
			Pager pager = new Pager(persons.getTotalPages(),persons.getNumber(),BUTTONS_TO_SHOW);
			
			
			model.addAttribute("listaReservas", persons);
			model.addAttribute("selectedPageSize", evalPageSize);
			model.addAttribute("pageSizes", PAGE_SIZES);
			model.addAttribute("pager", pager);
			
		return "gestionReservasPendientes";
	}
	
	/**
	 * Controlador que elimina una reserva pendiente
	 * @param id
	 * @return
	 */
	@GetMapping("/gestionReservasPendientes/borrar/{id}")
	public String borrarReserva(@PathVariable("id") long id) {
		servicioReserva.deleteById(id);
		return "redirect:/gestionReservasPendientes";
	}
	
	/**
	 * Controlador que muestra la página en la que se encuentra la lista de todas las reservas
	 * finalizadas de todos los usuarios
	 * @param pageSize
	 * @param page
	 * @param model
	 * @return
	 */
	@GetMapping ({"/gestionReservasFinalizadas"})
	public String irAGestionReservasFinalizadas(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page,Model model) {
		
		// Evalúa el tamaño de página. Si el parámetro es "nulo", devuelve
		// el tamaño de página inicial.
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
						
		// Calcula qué página se va a mostrar. Si el parámetro es "nulo" o menor
		// que 0, se devuelve el valor inicial. De otro modo, se devuelve el valor
		// del parámetro decrementado en 1.
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
						
		// Obtenemos la página definida por evalPage y evalPageSize de objetos de
		// nuestro modelo
		Page<Reserva> persons = servicioReserva.listaTodasReservasFinalizadas(PageRequest.of(evalPage, evalPageSize));
						
		// Creamos el objeto Pager (paginador) indicando los valores correspondientes.
		// Este sirve para que la plantilla sepa cuantas páginas hay en total, cuantos
		// botones
		// debe mostrar y cuál es el número de objetos a dibujar.
		Pager pager = new Pager(persons.getTotalPages(),persons.getNumber(),BUTTONS_TO_SHOW);
		
		
		model.addAttribute("listaReservas", persons);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		return "gestionReservasFinalizadas";
	}
	
	/**
	 * Controlador que muestra la página en la que se encuentra la lista con todos los usuarios
	 * de la aplicación, tanto los verificados como los que no están verificados.
	 * @param pageSize
	 * @param page
	 * @param model
	 * @return
	 */
	@GetMapping ({"/gestionUsuariosVerificados"})
	public String irAGestionUsuariosVerificados(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page,Model model) {
		
		// Evalúa el tamaño de página. Si el parámetro es "nulo", devuelve
		// el tamaño de página inicial.
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
				
		// Calcula qué página se va a mostrar. Si el parámetro es "nulo" o menor
		// que 0, se devuelve el valor inicial. De otro modo, se devuelve el valor
		// del parámetro decrementado en 1.
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
				
		// Obtenemos la página definida por evalPage y evalPageSize de objetos de
		// nuestro modelo
		Page<Usuario> persons = servicioUsuario.findAllPageable(PageRequest.of(evalPage, evalPageSize));
				
		// Creamos el objeto Pager (paginador) indicando los valores correspondientes.
		// Este sirve para que la plantilla sepa cuantas páginas hay en total, cuantos
		// botones
		// debe mostrar y cuál es el número de objetos a dibujar.
		Pager pager = new Pager(persons.getTotalPages(),persons.getNumber(),BUTTONS_TO_SHOW);
				
		
		model.addAttribute("listaUsuariosV", persons);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		return "gestionUsuarios";
	}
	
	/**
	 * Controlador que borra un usuario mediante su id
	 * @param id
	 * @return
	 */
	@GetMapping ({"/gestionUsuariosVerificados/borrar/{id}"})
	public String cancelarSolicitudVerificacion(@PathVariable("id") long id) {
		
		servicioUsuario.deleteById(id);
		
		return "redirect:/gestionUsuariosVerificados";
	}
	
	/**
	 * Controlador que verifica un usuario al darle al botón de verificar
	 * @param id
	 * @return
	 */
	@GetMapping ({"/gestionUsuariosVerificados/verificar/{id}"})
	public String verificarSolicitud(@PathVariable("id") long id) {
		
		servicioUsuario.findById(id).setVerificado(true);
		servicioUsuario.save(servicioUsuario.findById(id));
		
		return "redirect:/gestionUsuariosVerificados";
	}
	
	//Pablo
	
	/**
	 * Controlador para navegar en las opciones del admin
	 * @param model
	 * @return
	 */
	@GetMapping({"/listusers"})
	public String iralistaUsuarios(Model model) {
			model.addAttribute("listaUsuarios", servicioUsuario.findAll());
		return "gestionUsuarios";
	}
	
	/**
	 * Controlador para navegar en las opciones del admin
	 * @param model
	 * @return
	 */
	@GetMapping({"/listaulas"})
	public String iralistaAulas(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page,Model model) {
			
			// Evalúa el tamaño de página. Si el parámetro es "nulo", devuelve
			// el tamaño de página inicial.
			int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
							
			// Calcula qué página se va a mostrar. Si el parámetro es "nulo" o menor
			// que 0, se devuelve el valor inicial. De otro modo, se devuelve el valor
			// del parámetro decrementado en 1.
			int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
							
			// Obtenemos la página definida por evalPage y evalPageSize de objetos de
			// nuestro modelo
			Page<Espacio> persons = servicioEspacio.findAllPageable(PageRequest.of(evalPage, evalPageSize));
							
			// Creamos el objeto Pager (paginador) indicando los valores correspondientes.
			// Este sirve para que la plantilla sepa cuantas páginas hay en total, cuantos
			// botones
			// debe mostrar y cuál es el número de objetos a dibujar.
			Pager pager = new Pager(persons.getTotalPages(),persons.getNumber(),BUTTONS_TO_SHOW);
				
			model.addAttribute("listaAulas", persons);
			model.addAttribute("selectedPageSize", evalPageSize);
			model.addAttribute("pageSizes", PAGE_SIZES);
			model.addAttribute("pager", pager);
		
		return "menuAdmin2";
		
	}
	
	/**
	 * Controlador para navegar en las opciones del admin
	 * @param model
	 * @return
	 */
	@GetMapping({"/listareservas"})
	public String iralistaReservas(Model model) {
		return "gestionReservasPendientes";
		
	}
	
	
	

}
