package com.salesianostriana.dam.gestiondeaulas.controller;

import java.security.Principal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.salesianostriana.dam.gestiondeaulas.model.Espacio;
import com.salesianostriana.dam.gestiondeaulas.model.Reserva;
import com.salesianostriana.dam.gestiondeaulas.service.ConfiguracionService;
import com.salesianostriana.dam.gestiondeaulas.service.EspacioServicio;
import com.salesianostriana.dam.gestiondeaulas.service.FechaEspecialServicio;
import com.salesianostriana.dam.gestiondeaulas.service.ReservaServicio;
import com.salesianostriana.dam.gestiondeaulas.service.UsuarioServicio;


/**
* Controlador para establecer las rutas principales de la aplicacion
* @author Daniel Leiva Nieto
*
*/
@Controller
public class ControladorReservas {
	
	@Autowired
	UsuarioServicio servicioUsuario;
	
	@Autowired
	ReservaServicio servicioReserva;
	
	@Autowired
	EspacioServicio servicioEspacio;
	
	@Autowired
	ConfiguracionService servicioConfig;
	
	@Autowired
	FechaEspecialServicio servicioFechaEspecial;
	
	/**
	 * Controlador que según el id borra la reserva del usuario
	 * @param id
	 * @return
	 */
	@GetMapping("/borrarReserva/{id}")
	public String borrarReserva(@PathVariable("id") long id) {
		servicioReserva.deleteById(id);
		return "redirect:/inicio";
	}
	
	/**
	 * Controlador que te dirige a la página de añadir reserva
	 * @param model
	 * @return
	 */
	@GetMapping ({"/reservarespacio"})
	public String irAReservarAula(Model model) {
		
		Reserva reserva = new Reserva();
		model.addAttribute("listaRangoHorario", servicioReserva.getListaRangoHorario());
		model.addAttribute("reservaNueva", reserva);
		
		return "reservar";
	}
	
	/**
	 * Muestra las pistas disponibles según la hora y fecha indicada en reservar espacio por el usuario.
	 * @param rh
	 * @param model
	 * @return
	 */
	@PostMapping ("/reservarHora")
	public String reservaPaso2(@ModelAttribute("reservaNueva") Reserva reserva,  Model model) {
		
		List<Espacio> listaEspacios = servicioReserva.buscarEspaciosDisponiblesParaFechaYHora(reserva.getFecha(), reserva.getHora(), servicioEspacio);
		
		if (listaEspacios.isEmpty() 
				|| servicioConfig.isDiaNoHabil(reserva.getFecha()) 
				|| reserva.getFecha().isBefore(LocalDate.now())
				|| (reserva.getFecha().isEqual(LocalDate.now()) && reserva.getHora().isBefore(LocalTime.now()))
				|| servicioFechaEspecial.isFechaEspecial(reserva.getFecha())) {
				
			model.addAttribute("listaRangoHorario", servicioReserva.getListaRangoHorario());
			model.addAttribute("reservaNueva", reserva);
			return "reservarFallo";
			
			
		}else {
			
			model.addAttribute("listaEspacios",listaEspacios);
			
			return "reservar2";
		}
		
	}
	
	/**
	 * Muestra las pistas disponibles según la hora y fecha indicada en reservar espacio por el usuario.
	 * @param rh
	 * @param model
	 * @return
	 */
	@PostMapping ("/reservarAula")
	public String reservaPaso3(@ModelAttribute("reservaNueva") Reserva reserva,  Model model,Principal principal) {
		
		reserva.setUsuario(servicioUsuario.buscarUsuarioLogeado(principal));
		
		servicioReserva.save(reserva);
		
		return "redirect:/inicio";
	}
	
	

}
