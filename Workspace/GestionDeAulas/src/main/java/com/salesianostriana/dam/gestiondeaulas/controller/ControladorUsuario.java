package com.salesianostriana.dam.gestiondeaulas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.salesianostriana.dam.gestiondeaulas.model.Usuario;
import com.salesianostriana.dam.gestiondeaulas.service.UsuarioServicio;

/**
 * Controlador para establecer las rutas principales de la aplicacion
 * @author Pablo Rodríguez Roldán
 *
 */

@Controller
public class ControladorUsuario {
	
	@Autowired
	UsuarioServicio servicioUsuario;
	

	/**
	 * Método que borra un usuario por su Id
	 * 
	 * @param id id del usuario a borrar
	 * @return plantilla de lista de usuarios
	 */
	@GetMapping("/borrarUser/{id}")
	public String borrar(@PathVariable("id") long id) {
		for(Usuario usuario : servicioUsuario.findAll()) {
			if(usuario.getId() == id) {
				servicioUsuario.delete(usuario);
			}
		}
		
		return "redirect:/listusers";
	}
	
	

}
