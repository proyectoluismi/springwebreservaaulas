package com.salesianostriana.dam.gestiondeaulas.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Esta clase define los atributos necesarios para modelar la configuracion extra de la aplicación
 * @author Daniel Leiva Nieto
 *
 */
@Data @NoArgsConstructor
@Entity
public class Configuracion {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	/**
	 * Nombre dia no habil
	 */
	private String diaNoHabil;
	
	/**
	 * Número del sía de la semana (1-7)
	 */
	private int diaSemanaNum;
	
	/**
	 * Booleano para permitir al admin que se pueda reservar aulas los sabados
	 */
	private boolean habilitado;
	
	public Configuracion(String diaNoHabil,int diaSemanaNum, boolean habilitado) {
		super();
		this.diaNoHabil = diaNoHabil;
		this.diaSemanaNum = diaSemanaNum;
		this.habilitado = habilitado;
	}
	
	

}
