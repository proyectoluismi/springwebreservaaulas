/**
 * 
 */
package com.salesianostriana.dam.gestiondeaulas.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Daniel Leiva Nieto
 *
 */

@Data @NoArgsConstructor
@Entity
public class Espacio {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	/**
	 * Nombre del colegio
	 */
	private String nombreColegio;
	/**
	 * Nombre del aula
	 */
	private String nombreAula;
	/**
	 * Aforo máximo del aula
	 */
	private int aforo;
	/**
	 * Número de ordenadores en el aula
	 */
	private int numEquipos;
	
	/**
	 * Booleano para habilitar o deshabilitar el espacio
	 */
	private boolean disponible;
	
	
	/**
	 * Constructor con todos los parámetros
	 * @param id
	 * @param nombreColegio
	 * @param nombreAula
	 * @param aforo
	 * @param numEquipos
	 */
	public Espacio( String nombreColegio, String nombreAula, int aforo, int numEquipos, boolean disponible) {
		super();
		this.nombreColegio = nombreColegio;
		this.nombreAula = nombreAula;
		this.aforo = aforo;
		this.numEquipos = numEquipos;
		this.disponible=disponible;
		
	}
	
	

}
