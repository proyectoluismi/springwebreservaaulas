package com.salesianostriana.dam.gestiondeaulas.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Esta clase define los atributos necesarios para modelar las fechas especiales en las que no se podrá reservar ningún aula
 * @author Daniel Leiva Nieto
 *
 */
@Data @NoArgsConstructor
@Entity
public class FechaEspecial {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/**
	 * Dia de la fecha especial
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private LocalDate fechaEspecial;
	
	/**
	 * Motivo para ser fecha especial
	 */
	private String descripcion;

	/**
	 * Constructor con todos los parámetros
	 * @param fechaEspecial
	 * @param descripcion
	 */
	public FechaEspecial(LocalDate fechaEspecial, String descripcion) {
		super();
		this.fechaEspecial = fechaEspecial;
		this.descripcion = descripcion;
	}

	
	
}
