package com.salesianostriana.dam.gestiondeaulas.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Esta clase define los atributos necesarios para modelar las reservas de las aulas
 * @author Daniel Leiva Nieto
 *
 */
@Data @NoArgsConstructor
@Entity
public class Reserva {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name = "espacio_id")
	private Espacio espacio;
	
	/**
	 * Fecha de la reserva del espacio
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private LocalDate fecha;
	
	/**
	 * Hora de la reserva
	 */
	@DateTimeFormat(iso = ISO.TIME)
	private LocalTime hora;

	/**
	 * Constructor con todos los parámetros
	 * @param id
	 * @param usuario
	 * @param espacio
	 * @param fechayHora
	 */
	public Reserva( Usuario usuario, Espacio espacio, LocalDate fecha, LocalTime hora) {
		super();
		this.usuario = usuario;
		this.espacio = espacio;
		this.fecha = fecha;
		this.hora=hora;
	}

}
