package com.salesianostriana.dam.gestiondeaulas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
@Entity
public class Usuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	/**
	 * Nombre del usuario
	 */
	private String nombre;
	/**
	 * Apellidos del usuario
	 */
	private String apellidos;
	/**
	 * Contraseña del usuario
	 */
	private String password;
	/**
	 * Correo eléctronico del usuario
	 */
	@Column(unique = true)
	private String email;
	/**
	 * Booleano para diferenciarlo entre usuario y admin
	 */
	private boolean admin;
	/**
	 * Departamento al que pertenece el profesor
	 */
	private String departamento;
	/**
	 * Booleano para comprobar si el admin ha aceptado su solicitud de registro
	 */
	private boolean verificado;
	
	
	/**
	 * Constructor con todos los parámetros
	 * @param id
	 * @param nombre
	 * @param apellidos
	 * @param password
	 * @param email
	 * @param admin
	 * @param departamento
	 * @param verificado
	 */
	public Usuario(String nombre, String apellidos, String password, String email, boolean admin,
			String departamento, boolean verificado) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.password = password;
		this.email = email;
		this.admin = admin;
		this.departamento = departamento;
		this.verificado = verificado;
	}
	
	
	
	

}
