package com.salesianostriana.dam.gestiondeaulas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesianostriana.dam.gestiondeaulas.model.Configuracion;

/**
 * Repositorio de la configuración de la aplicación
 * 
 * @author Daniel Leiva Nieto
 *
 */
public interface ConfiguracionRepositorio extends JpaRepository<Configuracion,Long>{
	
	

}
