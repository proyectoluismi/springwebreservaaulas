package com.salesianostriana.dam.gestiondeaulas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesianostriana.dam.gestiondeaulas.model.Espacio;

/**
 * Repositorio de los espacios disponibles de los colegios
 * 
 * @author Daniel Leiva Nieto
 *
 */
public interface EspacioRepositorio extends JpaRepository<Espacio, Long>{

}
