package com.salesianostriana.dam.gestiondeaulas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesianostriana.dam.gestiondeaulas.model.FechaEspecial;


/**
 * Repositorio de las fechas especiales
 * 
 * @author Daniel Leiva Nieto
 *
 */
public interface FechaEspecialRepositorio extends JpaRepository<FechaEspecial, Long>{

}
