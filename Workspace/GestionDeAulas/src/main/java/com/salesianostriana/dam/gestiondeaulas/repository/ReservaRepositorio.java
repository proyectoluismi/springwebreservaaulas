package com.salesianostriana.dam.gestiondeaulas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salesianostriana.dam.gestiondeaulas.model.Reserva;

/**
 * Repositorio de las reservas realizadas
 * 
 * @author Daniel Leiva Nieto
 *
 */
public interface ReservaRepositorio extends JpaRepository<Reserva, Long>{

}
