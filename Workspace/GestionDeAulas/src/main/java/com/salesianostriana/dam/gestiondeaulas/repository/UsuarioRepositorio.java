package com.salesianostriana.dam.gestiondeaulas.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.salesianostriana.dam.gestiondeaulas.model.Usuario;

/**
 * Repositorio de los usuarios de la aplicación
 * 
 * @author Daniel Leiva Nieto
 *
 */
public interface UsuarioRepositorio extends JpaRepository<Usuario, Long>{
	
	/**
	 * Encuentra un usuario por su email
	 * @param email
	 * @return
	 */
	public Usuario findFirstByEmail(String email);
	
	/**
	 * Busca todos aquellos usuarios que se han registrado, ordenándolos de manera que aparezcan
	 * primero aquellos usuarios que aún no están verificados
	 * @param pageable
	 * @return
	 */
	public Page<Usuario> findByOrderByVerificadoAsc(Pageable pageable);
}
