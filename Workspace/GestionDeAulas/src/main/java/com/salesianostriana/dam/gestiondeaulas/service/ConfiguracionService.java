package com.salesianostriana.dam.gestiondeaulas.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.salesianostriana.dam.gestiondeaulas.model.Configuracion;
import com.salesianostriana.dam.gestiondeaulas.repository.ConfiguracionRepositorio;
import com.salesianostriana.dam.gestiondeaulas.service.base.ServicioBase;

/**
 * Servicio de la entidad Usuario, aquí irán todas las funcionalidades de la entidad.
 * @author Daniel Leiva Nieto
 *
 */

@Service
public class ConfiguracionService extends ServicioBase<Configuracion, Long, ConfiguracionRepositorio>{
	
	/**
	 * Método que recibe como parámetro una fecha y devuelve verdadero si en la fecha indicada no se puede reservar aula
	 * @param fecha
	 * @return
	 */
	public boolean isDiaNoHabil(LocalDate fecha) {
		
		boolean resul = false;
		for (Configuracion c : repositorio.findAll()) {
			if (fecha.getDayOfWeek().getValue() == c.getDiaSemanaNum() && !c.isHabilitado()) {
				resul = true;
			}
		}
		
		return resul;
	}
	
}
