package com.salesianostriana.dam.gestiondeaulas.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.salesianostriana.dam.gestiondeaulas.model.Espacio;
import com.salesianostriana.dam.gestiondeaulas.repository.EspacioRepositorio;
import com.salesianostriana.dam.gestiondeaulas.service.base.ServicioBase;

/**
 * Servicio de la entidad Espacio, aquí irán todas las funcionalidades de la entidad.
 * @author Daniel Leiva Nieto
 *
 */
@Service
public class EspacioServicio extends ServicioBase<Espacio, Long, EspacioRepositorio>{
	
	/**
	 * Método que pagina la lista de todos los usuarios de la base de datos
	 * @param pageable Objeto tipo pageable
	 * @return Lista paginada de todos los usuarios
	 */
	public Page<Espacio> findAllPageable(Pageable pageable) {
        return repositorio.findAll(pageable);
    }

}
