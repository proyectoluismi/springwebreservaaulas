package com.salesianostriana.dam.gestiondeaulas.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.salesianostriana.dam.gestiondeaulas.model.FechaEspecial;
import com.salesianostriana.dam.gestiondeaulas.repository.FechaEspecialRepositorio;
import com.salesianostriana.dam.gestiondeaulas.service.base.ServicioBase;

/**
 * Servicio de la entidad FechaEspecial, aquí irán todas las funcionalidades de la entidad.
 * @author Daniel Leiva Nieto
 *
 */
@Service
public class FechaEspecialServicio extends ServicioBase<FechaEspecial, Long, FechaEspecialRepositorio>{
	
	/**
	 * Método que recibe como parámetro una fecha y devuelve verdadero si en la fecha indicada no se puede reservar aula
	 * @param fecha
	 * @return
	 */
	public boolean isFechaEspecial(LocalDate fechaIndicada) {
		boolean resul = false;
		for (FechaEspecial f : repositorio.findAll()) {
			if (fechaIndicada.equals(f.getFechaEspecial())) {
				resul = true;
			}
		}
		return resul;
	}
	
	

}
