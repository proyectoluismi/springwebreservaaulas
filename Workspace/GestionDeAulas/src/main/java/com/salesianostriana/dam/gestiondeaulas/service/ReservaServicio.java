package com.salesianostriana.dam.gestiondeaulas.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.salesianostriana.dam.gestiondeaulas.model.Espacio;
import com.salesianostriana.dam.gestiondeaulas.model.Reserva;
import com.salesianostriana.dam.gestiondeaulas.model.Usuario;
import com.salesianostriana.dam.gestiondeaulas.repository.ReservaRepositorio;
import com.salesianostriana.dam.gestiondeaulas.service.base.ServicioBase;

/**
 * Servicio de la entidad Reserva, aquí irán todas las funcionalidades de la entidad.
 * @author Daniel Leiva Nieto
 *
 */
@Service
public class ReservaServicio extends ServicioBase<Reserva, Long, ReservaRepositorio>{
	
	private List<LocalTime> listaRangoHorario = new ArrayList<LocalTime>(Arrays.asList(LocalTime.of(8, 0),
			LocalTime.of(9, 0),LocalTime.of(10, 0),LocalTime.of(11, 30),LocalTime.of(12, 30),LocalTime.of(13, 30)));
	
	
	public List<LocalTime> getListaRangoHorario() {
		return listaRangoHorario;
	}
	public void setListaRangoHorario(List<LocalTime> listaRangoHorario) {
		this.listaRangoHorario = listaRangoHorario;
	}
	
	/**
	 * Este método devuelve una lista con todas las reservas del usuario logeado
	 * @param servicioUsuario
	 * @param principal
	 * @return
	 */
	public List<Reserva> buscarReservasPorUsuario(Usuario usuario){
		
		List<Reserva> listaReservas = new ArrayList<>();
		
		for (Reserva r : repositorio.findAll()) {
			if (usuario.getId()==r.getUsuario().getId()) {
				listaReservas.add(r);
			}
		}
		
		return listaReservas;
	}
	/**
	 * Este método devuelve una lista con todas las reservas pendientes del usuario logeado
	 * @param servicioUsuario
	 * @param principal
	 * @return
	 */
	public List<Reserva> buscarReservasPendientesPorUsuario(Usuario usuario){
		
		List<Reserva> listaReservas = new ArrayList<>();
		
		for (Reserva r : repositorio.findAll()) {
			if (r.getFecha().isAfter(LocalDate.now())) {
				if (usuario.getId()==r.getUsuario().getId()) {
				listaReservas.add(r);
				}
			}
		}
		
		Collections.sort(listaReservas, Comparator.comparing(x -> x.getFecha()));
		
		return listaReservas;
		
	}
	
	/**
	 * Este método devuelve una lista con todas las reservas pendientes del usuario logeado
	 * @param servicioUsuario
	 * @param principal
	 * @return
	 */
	public List<Reserva> buscarReservasPasadasPorUsuario(Usuario usuario){
		
		List<Reserva> listaReservas = new ArrayList<>();
		
		for (Reserva r : repositorio.findAll()) {
			if (r.getFecha().isBefore(LocalDate.now())) {
				if (usuario.getId()==r.getUsuario().getId()) {
				listaReservas.add(r);
				}
			}
		}
		
		Collections.sort(listaReservas, Comparator.comparing(x -> x.getFecha()));
		Collections.reverse(listaReservas);
		return listaReservas;
		
	}
	/**
	 * Este método devuelve una lista de reservas ocupadas filtrada por fecha y hora
	 * @param fecha
	 * @param hora
	 * @return
	 */
	public List<Reserva> buscarReservasOcupadasParaFechaYHora(LocalDate fecha, LocalTime hora) {
		
		List<Reserva> listaReservas = new ArrayList<>();
		
		for (Reserva r : repositorio.findAll()) {
			if (r.getFecha().equals(fecha) && r.getHora().equals(hora)) {
				listaReservas.add(r);
			}
		}
		
		return listaReservas;
		
	}
	
	/**
	 * Este método devuelve la lista de espacios disponibles para un dia y una hora en concreto
	 * @param fecha
	 * @param hora
	 * @param servicioEspacio
	 * @return
	 */
	public List<Espacio> buscarEspaciosDisponiblesParaFechaYHora(LocalDate fecha, LocalTime hora, EspacioServicio servicioEspacio) {
		
		List<Espacio> listaEspacios = servicioEspacio.findAll();
		
		/*
		 * for (Iterator<Integer> iterator = integers.iterator(); iterator.hasNext();) {
    		Integer integer = iterator.next();
    			if(integer == 2) {
        			iterator.remove();
    			}
			}	
		 */
		
		for (Reserva r : buscarReservasOcupadasParaFechaYHora(fecha, hora)) {
			for (Iterator<Espacio> iterator = listaEspacios.iterator(); iterator.hasNext();) {
				Espacio espacio = iterator.next();
				if (r.getEspacio().equals(espacio)) {
					iterator.remove();
				}
			}
		}
		
		for (Iterator<Espacio> iterator = listaEspacios.iterator(); iterator.hasNext();) {
			Espacio espacio = iterator.next();
			if (!espacio.isDisponible()) {
				iterator.remove();
			}
		}
		
		
		/*for (Reserva r : buscarReservasOcupadasParaFechaYHora(fecha, hora)) {
			for (Espacio espacio : listaEspacios) {
				if (r.getEspacio().equals(espacio)) {
					listaEspacios.remove(espacio);
				}
			}
				
			}*/
		return listaEspacios;
		
	}
	
	/**
	 * Este método devuelve una lista con todas las reservas pendientes del usuario logeado
	 * @param servicioUsuario
	 * @param principal
	 * @return
	 */
	public List<Reserva> buscarTodasReservasPendientes(){
		
		List<Reserva> listaReservas = new ArrayList<>();
		
		for (Reserva r : repositorio.findAll()) {
			if (r.getFecha().isAfter(LocalDate.now())) {
				listaReservas.add(r);
			}
		}
		
		Collections.sort(listaReservas, Comparator.comparing(x -> x.getFecha()));
		
		return listaReservas;
		
	}
	
	/**
	 * Este método devuelve una lista con todas las reservas pendientes del usuario logeado
	 * @param servicioUsuario
	 * @param principal
	 * @return
	 */
	public List<Reserva> buscarTodasReservasPasadas(){
		
		List<Reserva> listaReservas = new ArrayList<>();
		
		for (Reserva r : repositorio.findAll()) {
			if (r.getFecha().isBefore(LocalDate.now())) {
				listaReservas.add(r);
			}
		}
		
		Collections.sort(listaReservas, Comparator.comparing(x -> x.getFecha()));
		Collections.reverse(listaReservas);
		return listaReservas;
		
	}
	
	/**
	 * Este método devuelve un Page con la lista de todas las reservas pasadas
	 * @param pageable
	 * @return
	 */
	public Page<Reserva> listaTodasReservasFinalizadas(Pageable pageable) {
		
		Page<Reserva> list = repositorio.findAll(pageable);
        
		for (Iterator<Reserva> iterator = list.iterator(); iterator.hasNext();) {
			Reserva r = iterator.next();
			if (r.getFecha().isAfter(LocalDate.now()) || (r.getFecha().isEqual(LocalDate.now()) && r.getHora().isAfter(LocalTime.now()))) {
				iterator.remove();
			}
		}
		
		return list;
		
    }
	
	/**
	 * Este método devuelve un Page con la lista de todas las reservas pendientes
	 * @param pageable
	 * @return
	 */
	public Page<Reserva> listaTodasReservasPendientes(Pageable pageable) {
		
		Page<Reserva> list = repositorio.findAll(pageable);
        
		for (Iterator<Reserva> iterator = list.iterator(); iterator.hasNext();) {
			Reserva r = iterator.next();
			if (r.getFecha().isBefore(LocalDate.now()) || (r.getFecha().isEqual(LocalDate.now()) && r.getHora().isBefore(LocalTime.now()))) {
				iterator.remove();
			}
		}
		
		return list;
		
    }
	
	

}
