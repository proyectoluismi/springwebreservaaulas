package com.salesianostriana.dam.gestiondeaulas.service;

import java.security.Principal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.salesianostriana.dam.gestiondeaulas.model.Usuario;
import com.salesianostriana.dam.gestiondeaulas.repository.UsuarioRepositorio;
import com.salesianostriana.dam.gestiondeaulas.service.base.ServicioBase;

/**
 * Servicio de la entidad Usuario, aquí irán todas las funcionalidades de la entidad.
 * @author Daniel Leiva Nieto
 *
 */

@Service
public class UsuarioServicio extends ServicioBase<Usuario, Long, UsuarioRepositorio>{
	
	/**
	 * Este método devuelve un Usuario filtrado por su email
	 * @param email
	 * @return
	 */
	public Usuario buscarPorEmail(String email) {
		return repositorio.findFirstByEmail(email);
	}
	
	/**
	 * Este método sirve para proporcionar a la aplicación todos los datos del usuario logeaado, si el 
	 * usuario está logeado devuelve el usuario completo.
	 * @param model
	 * @param principal
	 * @return
	 */
	public Usuario buscarUsuarioLogeado(Principal principal) {
		
		if (principal!=null) {
			String email = principal.getName() ;
			Usuario u= buscarPorEmail(email);
			
			
			return u;
		}else {
			return null;
		}
		
	}
	
	/**
	 * Método que pagina la lista de todos los usuarios de la base de datos
	 * @param pageable Objeto tipo pageable
	 * @return Lista paginada de todos los usuarios
	 */
	public Page<Usuario> findAllPageable(Pageable pageable) {
        return repositorio.findByOrderByVerificadoAsc(pageable);
    }
	
}
