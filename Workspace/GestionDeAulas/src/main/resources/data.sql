insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Admin', 'admin', 'admin', 'admin@admin.com', true, 'Administrador', true)
;
insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Usuario', 'usuario', '1234', 'usuario@usuario.com', false, 'Usuario', true)
;
insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Lannie', 'Rumble', 'XeqkiH', 'lrumble2@berkeley.edu', false, 'Accounting', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Rycca', 'Fernely', 'HiisXvx8', 'rfernely3@wikispaces.com', true, 'Sales', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Lina', 'Treverton', 'rT33N19t', 'ltreverton4@amazon.de', true, 'Support', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Coop', 'Pierton', 'si5E2iythnX', 'cpierton5@aboutads.info', true, 'Training', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Tabby', 'Hazlehurst', 'gBNdY87f', 'thazlehurst6@archive.org', true, 'Sales', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Goldie', 'Callen', 'IKmJijtjzHQ', 'gcallen7@cafepress.com', true, 'Training', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Donal', 'Molyneaux', 'uC3ZhPu', 'dmolyneaux8@squarespace.com', false, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Joseph', 'Wellington', 'tzPKMEgxWkfj', 'jwellington9@aboutads.info', true, 'Support', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Agustin', 'Hanaby', 'klCDlZf7pF', 'ahanabya@indiatimes.com', false, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Martyn', 'Barkaway', 'avOAzSQs4Fc', 'mbarkawayb@nifty.com', true, 'Support', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Adara', 'Le Fevre', '77SG7ppK', 'alefevrec@instagram.com', false, 'Accounting', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Shandee', 'Muncer', 'ITJUTqxbdAO', 'smuncerd@pagesperso-orange.fr', false, 'Services', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Jamaal', 'Hurcombe', '16twvZV3pnh', 'jhurcombee@abc.net.au', false, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Kalie', 'Holwell', 'kjWaDMDyQTsG', 'kholwellf@hatena.ne.jp', false, 'Marketing', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Vanya', 'Miche', 'ULOeYKuJM', 'vmicheg@archive.org', true, 'Sales', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Edna', 'Prantoni', 'mgW567N', 'eprantonih@soup.io', false, 'Training', true);
insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Bern', 'Causer', 'bVCG7b', 'bcauseri@irs.gov', true, 'Legal', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Cletus', 'Coslett', 'xtTCrQA', 'ccoslettj@wisc.edu', true, 'Sales', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Sibilla', 'D''Acth', 'qR15rQXO6', 'sdacthk@disqus.com', false, 'Training', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Sharline', 'Climance', '883RHa', 'sclimancel@addtoany.com', true, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Lucila', 'Elven', 'yR1u3K', 'lelvenm@theatlantic.com', true, 'Training', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Antonella', 'Slyde', 'yWXuUF', 'aslyden@hexun.com', false, 'Training', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Bobbye', 'Lipp', 'h4wnViZc', 'blippo@odnoklassniki.ru', false, 'Marketing', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Ulrick', 'Plum', 'p3OemspJ1J', 'uplump@amazon.de', true, 'Human Resources', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Siobhan', 'Seth', 'gSfbSAa', 'ssethq@zimbio.com', true, 'Support', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Lambert', 'Calrow', 'tPh7E7pC8n', 'lcalrowr@usnews.com', true, 'Engineering', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Jose', 'Slany', 'meR0cR2BbbP', 'jslanys@gov.uk', true, 'Engineering', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Nelly', 'Gilbard', 'HhoCTbmem', 'ngilbardt@tiny.cc', false, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Ambur', 'Guilford', 'KFlKvU0', 'aguilfordu@hao123.com', false, 'Services', false);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Jareb', 'Keemar', 'LvhvnK', 'jkeemarv@twitpic.com', false, 'Accounting', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Marjory', 'Ragg', 'RqxepBbQ1', 'mraggw@topsy.com', false, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Karine', 'Ivakhnov', '7UiFcGPVPvl', 'kivakhnovx@constantcontact.com', true, 'Training', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Vanda', 'Leteurtre', 'ZnxqaVx', 'vleteurtrey@bravesites.com', true, 'Legal', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Florry', 'Andrault', '9XdWK8EdWG', 'fandraultz@ocn.ne.jp', false, 'Legal', false);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Ram', 'Winpenny', 'D7M1qU9e6', 'rwinpenny10@dailymail.co.uk', true, 'Sales', false);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Ed', 'Kristof', 'VMNh7HB8RI', 'ekristof11@timesonline.co.uk', true, 'Business Development', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Shaun', 'Bolderstone', 'wB3bNMZeVa', 'sbolderstone12@simplemachines.org', true, 'Human Resources', true);

insert into USUARIO (id, nombre, apellidos, password, email, admin, departamento, verificado) values (HIBERNATE_SEQUENCE.NEXTVAL, 'Harmonia', 'Simioni', 'kWe15VT', 'hsimioni13@rambler.ru', true, 'Business Development', true);




insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'20',True,'Aula-09','Salesianos','23');
insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'26',False,'Aula-10','Salesianos','20');
insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'22',True,'Aula-11','Salesianos','16');
insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'28',True,'Aula-12','Salesianos','18');
insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'30',False,'Aula-14','Salesianos','27');
insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'18',True,'Aula-18','Salesianos','22');
insert into ESPACIO(id,aforo,disponible,nombre_aula,nombre_colegio,num_equipos) values (HIBERNATE_SEQUENCE.NEXTVAL,'20',True,'Aula-06','Salesianos','17');

insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('10/10/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'44','1');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('13/10/2019', 'DD/MM/YYYY'), TO_DATE('12:00', 'HH24:MM'),'43','1');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('16/10/2019', 'DD/MM/YYYY'), TO_DATE('09:00', 'HH24:MM'),'44','1');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('01/10/2019', 'DD/MM/YYYY'), TO_DATE('11:00', 'HH24:MM'),'41','1');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('15/09/2019', 'DD/MM/YYYY'), TO_DATE('14:00', 'HH24:MM'),'42','1');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('20/09/2019', 'DD/MM/YYYY'), TO_DATE('08:00', 'HH24:MM'),'47','1');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('20/09/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'45','1');

insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('11/10/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'44','2');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('12/10/2019', 'DD/MM/YYYY'), TO_DATE('12:00', 'HH24:MM'),'43','2');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('09/10/2019', 'DD/MM/YYYY'), TO_DATE('11:00', 'HH24:MM'),'47','2');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('01/10/2019', 'DD/MM/YYYY'), TO_DATE('11:00', 'HH24:MM'),'41','2');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('13/09/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'43','2');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('11/09/2019', 'DD/MM/YYYY'), TO_DATE('09:00', 'HH24:MM'),'47','2');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('25/09/2019', 'DD/MM/YYYY'), TO_DATE('14:00', 'HH24:MM'),'45','2');

insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('21/10/2019', 'DD/MM/YYYY'), TO_DATE('08:00', 'HH24:MM'),'47','12');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('18/10/2019', 'DD/MM/YYYY'), TO_DATE('10:00', 'HH24:MM'),'44','21');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('09/09/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'41','22');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('01/09/2019', 'DD/MM/YYYY'), TO_DATE('12:00', 'HH24:MM'),'44','36');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('15/09/2019', 'DD/MM/YYYY'), TO_DATE('09:00', 'HH24:MM'),'46','17');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('20/10/2019', 'DD/MM/YYYY'), TO_DATE('08:00', 'HH24:MM'),'47','23');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('14/09/2019', 'DD/MM/YYYY'), TO_DATE('10:00', 'HH24:MM'),'45','24');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('10/10/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'44','34');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('13/10/2019', 'DD/MM/YYYY'), TO_DATE('12:00', 'HH24:MM'),'43','38');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('09/10/2019', 'DD/MM/YYYY'), TO_DATE('09:00', 'HH24:MM'),'46','34');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('01/10/2019', 'DD/MM/YYYY'), TO_DATE('10:00', 'HH24:MM'),'41','32');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('15/09/2019', 'DD/MM/YYYY'), TO_DATE('14:00', 'HH24:MM'),'43','20');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('22/10/2019', 'DD/MM/YYYY'), TO_DATE('11:00', 'HH24:MM'),'47','19');
insert into RESERVA(id,fecha,hora,espacio_id,usuario_id) values (HIBERNATE_SEQUENCE.NEXTVAL,TO_DATE('20/09/2019', 'DD/MM/YYYY'), TO_DATE('13:00', 'HH24:MM'),'45','21');

insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Lunes',1,true);
insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Martes',2,true);
insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Miercoles',3,true);
insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Jueves',4,true);
insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Viernes',5,true);
insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Sabado',6,false);
insert into CONFIGURACION(id,dia_no_habil,dia_semana_num,habilitado) values (HIBERNATE_SEQUENCE.NEXTVAL,'Domingo',7,false);

insert into FECHA_ESPECIAL(id,descripcion,fecha_Especial) values (HIBERNATE_SEQUENCE.NEXTVAL,'Halloween',TO_DATE('01/11/2019', 'DD/MM/YYYY'));
insert into FECHA_ESPECIAL(id,descripcion,fecha_Especial) values (HIBERNATE_SEQUENCE.NEXTVAL,'Inmaculada',TO_DATE('06/12/2019', 'DD/MM/YYYY'));

